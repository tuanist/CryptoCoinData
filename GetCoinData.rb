require 'open-uri'
require 'json'
require 'pg'
require 'time'

conn = PG::Connection.new( 'localhost', nil, nil, nil, 'coin', 'user', 'password' )

e = "CCCAGG"

time_epoch = Time.now.to_i

coinList = conn.exec("select Symbol as coin from coin_list c
                where c.marketcaprank is not null " ) do |result| 
#marketcap rank is obtained from cryptomarketcap on 3rd of february 2018

        result.each do |sym|
                p sym["coin"].delete(' ')
                fsym = sym["coin"].delete(' ')
                tsym = "USD"
                li = 2000 #maximum limit offerd by Cryptocompare
                
                checkAvailableData = "select max(time) AS time_max from data_hourly 
                                      where fsym = '#{fsym}' and tsym = '#{tsym}' and exchange = '#{e}'"
                timeLastUpdate =  conn.exec(checkAvailableData)
                time_interval = (time_epoch - timeLastUpdate[0]['time_max'].to_i)/3600 #3600s for hourly data
                li = time_interval if time_interval < li
                
                url = "https://min-api.cryptocompare.com/data/histohour?fsym=#{fsym}&tsym=#{tsym}&limit=#{li}&aggregate=1&e=#{e}"
                p url
                begin
                jsonBody = JSON.load(open(url))
                price = jsonBody["Data"]
                rescue
                        puts "rescue, retry"
                        sleep(10)
                        retry
                end
                #cnt = 0
                
                price = price.sort_by { |hash| hash['time'].to_i }
                
                checkDupe = "false"
                price.each do |data|
                        t = data["time"]
                        if checkDupe == "false" then
                            checkIfExistsString = "select count(time) AS cnt from data_hourly 
                                                where fsym = '#{fsym}' and tsym = '#{tsym}' and time = #{t} and exchange = '#{e}'"
                            #p checkIfExistsString
                            exists =  conn.exec(checkIfExistsString)
                            #p exists.values
                            #p exists[0]['cnt'].to_i
    
                            if exists[0]['cnt'].to_i > 0 then
                                    puts "data is duplicated and automatically be skipped"
                                    next
                                else 
                                    checkDupe = "true"
                            end
                        end
                        
                        c = data["close"]
                        h = data["high"]
                        l = data["low"]
                        o = data["open"]
                        vf = data["volumefrom"]
                        vt = data["volumeto"]
                        print t, c,h,l,o,vf,vt
                        p ""


                        sqlString = "INSERT INTO DATA_HOURLY(fsym, tsym, time, close, high, low, open,volumefrom, volumeto, exchange)
                                    VALUES ('#{fsym}', '#{tsym}', #{t}, #{c}, #{h}, #{l}, #{o}, #{vf}, #{vt}, '#{e}')"
                        conn.exec(sqlString)

                        #send some informative info to terminal
                end
        end
end
